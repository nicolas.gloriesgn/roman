# Roman

## Description

This project aims to get the roman number corresponding to a given number.

### exemple:
````bash
3 -> III
4 -> IV
````


## Structure

* ```src/app``` 
* ```config```
* ```bin```

## Installation

Clone with HTTPS: [https://gitlab.com/eliseraffin97/roman.git](https://gitlab.com/eliseraffin97/roman.git)



